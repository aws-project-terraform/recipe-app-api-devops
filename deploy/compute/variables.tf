# --- compute/variables.tf ---

variable "instance_count" {}
variable "instance_type" {}
variable "public_sg" {}
variable "public_subnets" {}
variable "vol_size" {}

variable "prefix" {
  default = "quanghuy"
}

variable "project" {
  default = "VTI project"
}

variable "contact" {
  default = "quanghuy2451999@gmail.com"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
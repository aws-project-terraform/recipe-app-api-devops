data "aws_ami" "server_ami" {
  most_recent = true
  owners      = ["137112412989"]
  filter {
    name   = "name"
    values = ["al2023-ami-2023.1.20230912.0-kernel-6.1-x86_64"]
  }

}

resource "random_id" "mtc_node_id" {
  byte_length = 2
  count       = var.instance_count
}


resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}




resource "aws_instance" "mtc_node" {
  count                  = var.instance_count
  instance_type          = var.instance_type
  ami                    = data.aws_ami.server_ami.id
  user_data              = file("./templates/bastion/user-data.sh")
  iam_instance_profile   = aws_iam_instance_profile.bastion.name
  vpc_security_group_ids = var.public_sg
  subnet_id              = var.public_subnets[count.index]
  key_name               = var.bastion_key_name
  # user_data              = ""

  root_block_device {
    volume_size = var.vol_size
  }

  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-EC2-instance"
    })
  )
}

output "bastion_host" {
  value = aws_instance.mtc_node.*.public_dns
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owners      = var.contact
    ManagedBy   = "terraform"
  }
}




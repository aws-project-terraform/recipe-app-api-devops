variable "excution_role_path" {}
variable "assume_role_path" {}

variable "prefix" {
  default = "quanghuy"
}

variable "project" {
  default = "VTI project"
}

variable "contact" {
  default = "quanghuy2451999@gmail.com"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default = "639136264313.dkr.ecr.ap-northeast-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default = "639136264313.dkr.ecr.ap-northeast-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  default = "changeme"
  description = "Secret key for Django app"
}

variable "aws_db_address" {}
variable "aws_db_name" {}
variable "aws_db_username" {}
variable "aws_db_password" {}
variable "aws_db_region" {}
variable "aws_vpc_id" {}
#variable "private_a" {}
#variable "private_c" {}
variable "public_a" {}
# variable "aws_s3_bucket" {}
#database/variable.tf

variable "db_instance_class" {}
variable "dbname" {}
variable "db_username" {}
variable "db_password" {}
variable "vpc_security_group_ids" {
}
variable "db_subnet_group_name" {
}
variable "db_engine_version" {}
variable "db_identifier" {}
variable "skip_db_snapshot" {}

variable "prefix" {
  default = "quanghuy"
}

variable "project" {
  default = "VTI project"
}

variable "contact" {
  default = "quanghuy2451999@gmail.com"
}
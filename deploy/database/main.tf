#database/main.tf

resource "aws_db_instance" "mtc_db" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = var.db_engine_version
  instance_class         = var.db_instance_class
  db_name                = var.dbname
  username               = var.db_username
  password               = var.db_password
  db_subnet_group_name   = var.db_subnet_group_name
  vpc_security_group_ids = var.vpc_security_group_ids
  identifier             = var.db_identifier
  skip_final_snapshot    = var.skip_db_snapshot
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-mtc-db-mysql"
    })
  )
}


#output

output "aws_db_address" {
  value = aws_db_instance.mtc_db.address
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owners      = var.contact
    ManagedBy   = "terraform"
  }
}

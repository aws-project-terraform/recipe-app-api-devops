#LB/variables.tf
variable "public_sg" {}
variable "private_subnets_a" {}
variable "private_subnets_c" {}
variable "tg_port" {}
variable "tg_protocol" {}
variable "vpc_id" {}
variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}
variable "listener_port" {}
variable "listener_protocol" {}


variable "prefix" {
  default = "quanghuy"
}

variable "project" {
  default = "VTI project"
}

variable "contact" {
  default = "quanghuy2451999@gmail.com"
}
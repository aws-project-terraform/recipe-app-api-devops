#LB/main.tf
resource "aws_lb" "mtc_lb" {
  name            = "mtc-loadbalancer"
  subnets         = [var.private_subnets_a,var.private_subnets_c]
  security_groups = var.public_sg
  idle_timeout    = 400
  tags = merge(
    local.common_tags,
    tomap({
      "Name" = "${local.prefix}-mtc-lb"
    })
  )
}

resource "aws_lb_target_group" "mtc_tg" {
  name     = "mtc-lb-tg-${substr(uuid(), 0, 3)}"
  port     = var.tg_port
  protocol = var.tg_protocol
  vpc_id   = var.vpc_id
  lifecycle {
    ignore_changes = [name]
    create_before_destroy = true
  }

  health_check {
    healthy_threshold   = var.elb_healthy_threshold
    unhealthy_threshold = var.elb_unhealthy_threshold
    timeout             = var.elb_timeout
    interval            = var.elb_interval
  }
}

resource "aws_lb_listener" "mtc_lb_listener" {
  load_balancer_arn = aws_lb.mtc_lb.arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.mtc_tg.arn
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owners      = var.contact
    ManagedBy   = "terraform"
  }
}

#output

output "aws_lb_dns" {
  value = aws_lb.mtc_lb.dns_name
}
# DESCRIPTION
 - there are 2 phases of this project: 
   + Phase 1: Provision a complete infrastructure with terraform on AWS (require to Using meta arguments: count, for each, depend on and lifecycle) 
   + phase 2: Phase 2 usage: setup CI/CD workflow based on GitLab to automate processes, deploy, mange and secure infrastructure

# OVERVIEW
![Alt text](archi.png)
# REQUIREMENTS
## Phase 1
### Provision below AWS services

|         Name      |                                   Description                                        |
|-------------------|--------------------------------------------------------------------------------------|
| IGW               | Internet Gateway for receiving requests from the internet.                           |
| Subnet            | 1 for public and 3 for private.                                                      |
| ECR               | Store image                                                                          |
| ECS               | Manage own servers/ECS manages Docker                                                |
| EC2               | Virtual server for admin                                                             |
| VPC               | Silo resources per environment, reduce risk of breaking things and improves security |
| Amazon RDS        | Create database for our app and managed solution                                     |
| ALB               | Accept/ Forward connections tasks running in ECS, zero downtime deployments          |
| Amazon S3         | Persistance storage to our app                                                       |
| Amazon CloudWatch | Store all the logs of our app                                                        |
| IAM               | Control access to AWS account, create user, policies to limit                        |

### Implementation
 ```bash
deploy/
├── compute
│   ├── main.tf
│   └── variables.tf
├── database
│   ├── main.tf
│   └── variables.tf
├── ecs
│   ├── main.tf
│   └── variables.tf
├── LB
│   ├── main.tf
│   └── variables.tf
├── networking
│   ├── main.tf
│   └── variables.tf
│
├── backend.tf
├── locals.tf
├── main.tf
├── s3.tf
├── variables.tf
└── provider.tf
 ```

## Phase 2 
### Workflow
![Alt text](work-flow-cicd.png)